class Stack
  
  def initialize
    self.head   = nil
    self.length = 0
  end

  def push data
    node = Node.new data
    if length == 0
      self.tail = node
    end
    node.next = self.head
    self.head = node
    self.length += 1
  end

  def pop
    return nil unless self.length > 0
      
    self.head = self.head.next
    self.tail = nil if self.length == 1
    self.length -= 1
  end

  def peek
    self.head
  end

  def clear
    while self.peek
      pop
    end
  end

  def each
    return nil unless block_given?
    current = self.head
    while current
      yield current
      current = current.next
    end
  end

  def print
    if self.length == 0
      puts "empty"
    else
      self.each { |node| puts node.data }
    end
  end

end