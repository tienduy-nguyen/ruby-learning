# Stack in Ruby

## About Stacks

- A stack is a data structure which you can use as a "to-do" list. You keep taking elements from the stack & processing them util the stack is empty.

- The big thing to notice here is that new items are added to the top of the stack. In a “Last-In First-Out” (LIFO) fashion. Meaning that when you take (pop) an item from the stack, it will be the last item that was pushed into it.

- When we add an element to Stack

  ```
  push(1)   push(2)   push(3)
  ---       ---       ---
    1         2         3
  ---       ---       ---
              1         2
            ---       ---
                        1
                      ---

  ```

- When we remove an item from a Stack
  
  ```
  pop    pop    pop
  ---    ---    ---
  3      2      1
  ---    ---    ---
  2      1
  ---    ---
  1     
  ---
  ```
- The safetest way to interact with the stack is through **push, peek, and pop** methods
  
## Stack Methods

### Initialize

- Constructor stack

  ```ruby
    def initialize
      self.head = nil
      self.length = 0
    end
  ```
### Push

- Add a new node into stack. This new node will move to the top of the list and becomes to the head of this list
  
  ```ruby
  def push data
    node = Node.new data
    if length == 0
      self.tail = node
    end
    node.next = self.head
    self.head = node
    self.length += 1
    
  end
  ```

### Pop

- Pop is a method to remove a first item from Stack
  
  ```ruby
  def pop data
    return nill unless self.length > 0
    self.head = self.head.next
    self.tail = nil if self.length  == 1
    self.length -= 1
  end

  ```

### Peek

- Return a head but do not remove it
  
  ```ruby
  def peek
    self.head
  end
  ```
### Clear
- Remove all elements in Stack
  ```ruby
  def clear
    while self.peek
      pop
    end
  end
  ```
### Each 

- Loop through all elements of Stack

  ```ruby
  def each
    return nil unless block_given?
    current = self.head
    while current
      yield current
      current = current.next
    end
  end
  ```
### Print

  ```ruby
    def print
    if self.length == 0
      puts "empty"
    else
      self.each { |node| puts node.data }
    end
    end
  ```
  