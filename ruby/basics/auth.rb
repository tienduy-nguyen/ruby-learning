users = [
  {username: "john1", password: "123"},
  {username: "john2", password: "123"},
  {username: "john3", password: "123"},
  {username: "john4", password: "123"},
  {username: "john5", password: "123"},
]

def auth_user(username, password, list_of_users)
  list_of_users.each do |user_record|
      if user_record[:username] == username && user_record[:password] == password
        return user_record
      end
    end
    return "Credentials were not correct"
end

puts "Welcome to the autheticator"
puts '-' * 100
puts "This program will take input from the user and compare password"
puts "If password is correct, you will get back the user object"

attemps = 1
while attemps < 4
  print "Username: "
  username = gets.chomp
  print "Password: "
  password= gets.chomp
  authentication = auth_user(username,password,users)
  puts authentication
  puts "Press n to quite or any other key to continue: "
  input = gets.chomp.downcase
  break if input =="n"
  attemps +=1
end
puts "You have exceeded the number of attempts!"
