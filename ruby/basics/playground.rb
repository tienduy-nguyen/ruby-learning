# Data type in ruby
# Bolleans, Symbols, Numbers, Strings, Arrays, Hashes
# Check data type of input: input.class

# Print in ruby
greeting = "Hello"

print greeting
puts greeting
p greeting

def sayHello
  puts "Hello Everybody"
end
def sayHello2(thing)
  puts thing
end

sayHello
sayHello2("Hi guys")

# String intepolation
firstName= "Felix"
lastName="Td"
fullName= "#{firstName} #{lastName}"
puts fullName

#Get input from terminal
# firstName=gets.chomp

#Print multi a character
puts "_" * 50

#Conversion string to number
str1 = "1"
num_1=str1.to_i
p str1
p num_1


#Convert data type in ruby
# to number: .to_i
# to string: .to_s
# to float : .to_f
# to symbol: .to_sym

#Comparison operator
# == != >= <=
# == Compare only value, ingore data type
# x.eql?(y) compare with data type

puts "*" * 100
# local variables & global variables
def method1
    
  def method2
              
      def method3
          m5, m6 = 3
          puts "Level 3"
          puts local_variables
      end            
      
      m3, m4 = 3
      puts "Level 2"
      puts local_variables
      method3    
  end        
  
  m1, m2 = 3
  puts "Level 1"
  puts local_variables
  method2
          
end

method1

puts "-" * 100

# Symbol in Ruby
p :name
p :name.class
p :name.methods.size
p "Jane".methods.size

p :name.object_id
p :name.object_id
p "name".object_id
p "name".object_id

# We compare the number of methods associated with instances of symbols and strings. A string has more than twice as many methods than symbol.

# Rational numbers
puts "-" * 100
puts 2.to_r
puts "23".to_r
puts 2.6.to_r

# nill value
puts "-" * 100
puts nil
p nil

p $val

p [1, 2, 3][4] 

p $val1 == $val2


# Array & Hashes
a = [:de, "Germany", :sk, "Slovakia", 
     :hu, "Hungary", :no, "Norway"]
p Hash[*a]


# Ruby freezing strings

msg = "Jane"
msg << " is " 
msg << "17 years old"

puts msg

msg.freeze

#msg << "and she is pretty"


# Ruby expressiong
puts '-' * 100
a = 10
b = 11
c = 12

puts a + b + c
puts c - a
puts a * b
puts c / 5
puts c % a
puts c ** a


# Exlamation mark
puts '-' * 100
chars = %w{a b c d e}

reversed_chars = chars.reverse
puts reversed_chars.inspect #[e d c b a]
puts chars.inspect #[a, b, c,d,e]

reversed_chars = chars.reverse!
puts reversed_chars.inspect #[e d c b a]
puts chars.inspect #[e d c b a]


# hash
puts '-' * 100
names1 = Hash.new

names1[1] = "Jane"
names1[2] = "Thomas"

names2 = Hash.new

names2[1] = "Robert"
names2[2] = "Julia"

names = names1.merge names2
puts names

names = names1.update names2
puts names