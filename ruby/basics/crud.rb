
require "bcrypt"

def create_hash_digest(password)
  BCrypt::Password.create(password)
end

def verify_hash_digest(password)
  BCrypt::Password.new(password)
end 

# new_password = create_hash_digest("1234567")
# puts new_password
# puts new_password == "1234567"

users = [
  {username: "john1", password: "123"},
  {username: "john2", password: "123"},
  {username: "john3", password: "123"},
  {username: "john4", password: "123"},
  {username: "john5", password: "123"},
]

def create_secure_users(list_of_users)
  list_of_users.each do |user_record|
    user_record[:password] =create_hash_digest(user_record[:password])
  end
  return list_of_users
end

new_users =  create_secure_users(users)
puts new_users

# users.each {|key, val| puts "User: #{key}  - Password: #{val}"}

# def authenticate_user(username, password, list_of_users)
#   if !list_of_users.any?{|h| h[:username] ==username}
#     return "User not found"
#   end
#   list_of_users.each do |user_record|
#     if user_record[:username] ==username && verify_hash_digest(user_record[:password]) ==password
#       return user_record
#     end
#     return "Credentials were not correct"
#   end
# end
def authenticate_user (username, password, list_of_users)
  list_of_users.each do |user_record|
    if user_record[:username] == username && verify_hash_digest(user_record[:password]) == password
      return user_record
    end 
  end
  return "Credentials were not correct"
end

puts '-' * 100
puts authenticate_user("john3", "123", new_users)
puts authenticate_user("john1", "123", new_users)

# my_password = BCrypt::Password.new("$2a$12$K0ByB.6YI2/OYrB4fQOYLe6Tv0datUVf6VZ/2Jzwm879BW5K1cHey")
# puts my_password == "my password"
# password_test =   BCrypt::Password.new("$2a$12$WCq2.O8B5jEcYKUZ2bmDpe/TjO7lIb9nn0qkC8rR6fHFdQWSI7qoe")
# puts password_test == "123"

