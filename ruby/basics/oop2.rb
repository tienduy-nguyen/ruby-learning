class Student
  attr_accessor :first_name, :last_name,:username,:email,:password

  def initialize(firstname, lastname, username, email,password)
    @first_name = firstname
    @last_name = lastname
    @username=username
    @email=email
    @password = password
  end


  def to_s
    puts "Student: #{first_name.capitalize} #{last_name.upcase}"
  end
end

st1  = Student.new("a","smith","asmith","a.smith@email.com","123")
st2 = Student.new("b","jordan","bjordan","a.jordan@email.com","123")
st1.to_s
st2.to_s