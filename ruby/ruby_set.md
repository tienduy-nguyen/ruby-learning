# Class set in ruby

## Definition

Ruby set is a collection of unordered values with no duplicates. This is a hybrid of Array's intuitive inter-operation facilities and Hash's fast lookup.

[Check out the officical document](https://ruby-doc.org/stdlib-2.7.1/libdoc/set/rdoc/Set.html)

## Ruby Set Examples

- Set is a class of Ruby which allow us to create an array with the unique values.

  For example, we have a large list of products with a lot of duplicates.  
 
  ```ruby
  require 'set'

  products = Set.new
  products.add(1)
  products.add(1)
  products.add(2)

  # Set: {1,2}
  ```

- Search in Set is very fast
  ```ruby
  products.include?(1)
  # true
  ```
## Difference between Set and Array

- Set: We can't get directly a valude of set

  ```ruby
  # undefined method '[]'
  ```
  However, we can easily convert a Set to an array

  ```ruby
  products.to_a
  [1,2]
  ```
- Set have two main characteristics:
  - Unique values
  - Quick search time

If we need this features for your list, you should use Set

- If we need an sorted Set, we can use SortedSet
  
  ```ruby
  sorted_numbers = SortedSet.new
  sorted_numbers << 5
  sorted_numbers << 2
  sorted_numbers << 1
  sorted_numbers
  # SortedSet: {1, 2, 5}
  ```