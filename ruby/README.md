# Ruby tutorial

## Table Of Contents

- [Ruby tutorial](#ruby-tutorial)
  - [Table Of Contents](#table-of-contents)
    - [RUBY BASICS](#ruby-basics)
      - [Print message to console](#print-message-to-console)
      - [Get message from console](#get-message-from-console)
      - [Variables](#variables)
      - [Variable interpolation](#variable-interpolation)
    - [RUBY LEXICAL STRUCTURAL](#ruby-lexical-structural)
      - [Ruby comments](#ruby-comments)
      - [Ruby constants](#ruby-constants)
      - [Ruby literal](#ruby-literal)
      - [Ruby blocks](#ruby-blocks)
      - [Ruby sigils](#ruby-sigils)
      - [Ruby operators](#ruby-operators)
      - [Ruby keywords](#ruby-keywords)
    - [RUBY DATA TYPES](#ruby-data-types)
      - [Ruby symbol](#ruby-symbol)
      - [Ruby rational Numbers](#ruby-rational-numbers)
      - [Ruby nill value](#ruby-nill-value)
      - [Ruby arrays & hashes](#ruby-arrays--hashes)
      - [Ruby conversions](#ruby-conversions)
    - [RUBY STRINGS](#ruby-strings)
      - [Ruby using quotes](#ruby-using-quotes)
      - [Ruby escape senquences](#ruby-escape-senquences)
      - [Ruby accessing string elements](#ruby-accessing-string-elements)
      - [Ruby concatenatin strings](#ruby-concatenatin-strings)
      - [Ruby freezing strings](#ruby-freezing-strings)
      - [Ruby comparing strings](#ruby-comparing-strings)
      - [Ruby string methods](#ruby-string-methods)
      - [Ruby formatting strings](#ruby-formatting-strings)
    - [RUBY STATEMENT](#ruby-statement)
      - [Ruby if statement](#ruby-if-statement)
      - [Ruby case statement](#ruby-case-statement)
      - [Ruby while, until statements](#ruby-while-until-statements)
      - [Ruby for statement](#ruby-for-statement)
      - [Ruby each method](#ruby-each-method)
      - [Ruby break, next statements](#ruby-break-next-statements)
      - [Ruby redo statement](#ruby-redo-statement)
    - [RUBY ARRAY](#ruby-array)
      - [Declarations and loop](#declarations-and-loop)
      - [Ruby array creationg](#ruby-array-creationg)
      - [Nested array](#nested-array)
      - [Ruby printing array contents](#ruby-printing-array-contents)
      - [Ruby array operations](#ruby-array-operations)
      - [Ruby modifying arrays](#ruby-modifying-arrays)
      - [Ruby reading array elements](#ruby-reading-array-elements)
      - [Ruby set operations](#ruby-set-operations)
      - [Ruby array select, collect, map methods](#ruby-array-select-collect-map-methods)
      - [Ruby array ordering elements](#ruby-array-ordering-elements)
    - [RUBY HASHES](#ruby-hashes)
      - [Basic creation](#basic-creation)
      - [Using key-value pairs](#using-key-value-pairs)
      - [Ruby hash iteration](#ruby-hash-iteration)
      - [Ruby deleting pairs in hash](#ruby-deleting-pairs-in-hash)
      - [Ruby adding elements to hashes](#ruby-adding-elements-to-hashes)
    - [RUBY OOP](#ruby-oop)
      - [Ruby constructor](#ruby-constructor)
      - [Ruby constructor overloading](#ruby-constructor-overloading)
      - [Ruby methods](#ruby-methods)
      - [Ruby access modifiers](#ruby-access-modifiers)
      - [Ruby inheritance](#ruby-inheritance)
      - [Ruby super method](#ruby-super-method)
      - [Ruby attribute accessors](#ruby-attribute-accessors)
      - [Ruby class constants](#ruby-class-constants)
      - [Ruby operator overloading](#ruby-operator-overloading)
      - [Ruby class methods](#ruby-class-methods)
      - [Ruby polymorphism](#ruby-polymorphism)
      - [Ruby modules](#ruby-modules)
      - [Ruby exceptions](#ruby-exceptions)
- [📃 License](#-license)
  - [:baby_chick:](#baby_chick)

<a name="basics"></a>

### RUBY BASICS

<a name="printconsole"></a>

#### Print message to console

```ruby
print "This is Ruby"
puts "This is Ruby"
p "This is Ruby"

print "What is your name? "
name = gets

puts "Hello #{name}"
```

<a name="getmessageconsole"></a>

#### Get message from console

```ruby
name=gets.chomp
puts "Hello #{name}"
```

#### Variables

```ruby
city = "New York"
name = "Paul"; age = 35
nationality = "American"

puts city
puts name
puts age
puts nationality

city = "London"

puts city
```

<a name="variableinterpolation"></a>

#### Variable interpolation

```ruby
age = 34
name = "William"

puts "#{name} is #{age} years old"
```

<a name="lexcial"></a>

### RUBY LEXICAL STRUCTURAL

<a name="comment"></a>

#### Ruby comments

- Two types of comments in Ruby: single line and multi-line

  ```ruby
  =begin
    comments.rb
    author Jan Bodnar
    ZetCode 2011
  =end

  # prints message to the terminal

  ```

  <a name="constant"></a>

#### Ruby constants

- An identifier with a first uppercase letter is a constant in Ruby.
  ```ruby
  Name="TienDuy"
  AGE = 26
  #Name, AGE is become a constant variable in Ruby
  ```

#### Ruby literal

- Example

  ```ruby
  name="James"
  puts "His name is #{name}
  sng=true
  if sng == true
      puts "He is single"
  else
      puts "He is in a relationship"
  end
  ```

#### Ruby blocks

- Ruby statements are often organized into blocks of code. A code block can be delimited using { } characters or do and end keywords.

  ```ruby
  puts [2, -1, -4, 0].delete_if { |x| x < 0 }

  [1, 2, 3].each do |e|
      puts e
  end
  ```

#### Ruby sigils

- Sigils $, @ are special characters that denote a scope in a variable. The $ is used for global variables, @ for instance variables and @@ for class variables.

  ```ruby
  tree_name = "pine"
  $car_name = "Peugeot"
  @sea_name = "Black sea"
  @@species = "Cat"

  p local_variables
  p global_variables.include? :$car_name
  p self.instance_variables
  p Object.class_variables

  ```

Check out on [zetcode variable](http://zetcode.com/lang/rubytutorial/variables/)

#### Ruby operators

- Operator using in Ruby
  ```
  !    +    -    ~    *    **    /    %
  <<    >>    &    |    ^
  ==    ===    !=    <=>    >=    >
  <    <=    =    %=    /=    -=
  +=    *=    **=    ..    ...    not
  and    or    ?:    &&    ||
  ```

#### Ruby keywords

- Avoid to declare the variables with these keyword
  ```
  alias    and      BEGIN      begin    break    case
  class    def      defined?   do       else     elsif
  END      end      ensure     false    for      if
  in       module   next       nil      not      or
  redo     rescue   retry      return   self     super
  then     true     undef      unless   until    when
  while    yield
  ```

<a name="dataType"></a>

### RUBY DATA TYPES

- Booleans
- Symbols
- Numbers
- Strings
- Arrays
- Hashes

  Check data type of input with .class

  ```ruby
  #!/usr/bin/ruby

  h = { :name => "Jane", :age => 17 }

  p true.class, false.class
  p "Ruby".class
  p 1.class
  p 4.5.class
  p 3_463_456_457.class
  p :age.class
  p [1, 2, 3].class
  p h.class
  ```

#### Ruby symbol

- Symbols are used to represent other objects. Using symbols instead of strings may save some resources. A symbol is an instance object of a Symbol class. Symbols are generated by using an colon before an identifier, like :name. Several objects also have to_sym methods. These methods convert those objects to symbols.

- A Ruby symbol cannot be changed at runtime. Ruby symbols are often used as hash keys, because we do not need full capabilities of a string objects for a key.

  ```ruby
  p :name
  p :name.class
  p :name.methods.size
  p "Jane".methods.size

  p :name.object_id
  p :name.object_id
  p "name".object_id
  p "name".object_id

  ```

  -We compare the number of methods associated with instances of symbols and strings. A string has more than twice as many methods than symbol.

- Symbols may be used as flags. Constants may be used in such situations as well. In C/C++ we would use enumerations.

  ```ruby
  light = :on

  if light == :on
      puts "The light is on"
  else
      puts "The light is off"
  end

  domains = {:sk => "Slovakia", :no => "Norway", :hu => "Hungary"}

  puts domains[:sk]
  puts domains[:no]
  puts domains[:hu]
  ```

#### Ruby rational Numbers

- Ruby supports rational numbers. A rational number is an exact number. Using rational numbers we avoid rounding errors

  ```ruby
  puts 2.to_r
  puts "23".to_r
  puts 2.6.to_r

  # Result:
  # 2/1
  # 23/1
  # 5854679515581645/2251799813685248
  ```

#### Ruby nill value

- Ruby has a special value nil. It is an absence of a value. The nil is a singleton object of a NilClass. There is only one nil; we cannot have more of it.

#### Ruby arrays & hashes

- Arrays and hashes are collections of objects. They group objects into one place.
- Arrays are ordered collections of objects. Hashes are collections of key-value pairs. We will have a single chapter for both arrays and hashes. The following example just gives a quick look at both containers.

  ```ruby
  nums = [1, 2, 3, 4]

  puts "There are #{nums.size} items in the array"

  nums.each do |num|
      puts num
  end

  domains = { :de => "Germany", :sk => "Slovakia",
  :us => "United States", :no => "Norway" }

  puts domains.keys
  puts domains.values

  ```

#### Ruby conversions

- Ruby has built-in conversion methods like to_i, to_s, to_f, to_r, to_c, to_a, to_sym
  ```ruby
  p "12".to_i
  p nil.to_i
  p 12.to_f
  p "11".to_f
  p "12".to_i
  p "13".to_f
  p "12".to_r
  p "13".to_c
  p "Jane".to_sym
  ```
- Convert a hash to an array
  ```ruby
  h = {:de => "Germany", :sk => "Slovakia"}
  p h.to_a
  ```
- Convert an array to hash
  ```ruby
  a = [:de, "Germany", :sk, "Slovakia",
     :hu, "Hungary", :no, "Norway"]
  p Hash[*a
  ```

<a name="string"></a>

### RUBY STRINGS

#### Ruby using quotes

```ruby
puts "There are many starts"
puts "He said, \"Which one is your favorite?\""
puts 'He said, "Which one is your favourite?"'
```

#### Ruby escape senquences

```ruby
puts "one\ntwo\nthree\nfour"
```

#### Ruby accessing string elements

```ruby
msg = "Ruby language"

puts msg["Ruby"]
puts msg["Python"]

puts msg[0]
puts msg[-1]

puts msg[0, 3]
puts msg[0..9]
puts msg[0, msg.length]
```

#### Ruby concatenatin strings

Ruby provides multiple ways of concatenating strings.

```ruby
lang = "Ruby" + " programming" + " languge"
puts lang

lang = "Python" " programming" " language"
puts lang

lang = "Perl" << " programming" << " language"
puts lang

lang = "Java".concat(" programming").concat(" language")
puts lang
```

#### Ruby freezing strings

String objects in Ruby have a freeze method, which makes them immutable. In Ruby, the strings are not immutable by default.

```ruby
msg = "Jane"
msg << " is "
msg << "17 years old"

puts msg

msg.freeze

#msg << "and she is pretty"
```

In this example, we demonstrate that strings can be modified. However, after calling the freeze method on a string object, we cannot modify the string anymore. If we uncomment the code line, we will get 'can't modify frozen string' error message.

#### Ruby comparing strings

```ruby
puts "aa" == "ab"
puts "Jane".eql? "Jan"

puts "a" <==> "b" #1
puts "b" <==> "a" #-1
puts "a" <==> "a" #0

# Compare strings regardless of the case
puts "Jane".casecmp "Jane" #0
puts "Jane".casecmp "jane" #0
puts "Jane".casecmp "Jan" #1
```

#### Ruby string methods

```ruby
word = "Determination"

puts "The word #{word} has #{word.size} characters"

puts word.include? "tion"
puts word.include? "tic"

puts

puts word.empty?
word.clear
puts word.empty?

ruby = "Ruby programming language"

puts ruby.upcase
puts ruby.downcase
puts ruby.capitalize
puts ruby.swapcase
```

```ruby
ws1 = "google.com"
ws2 = "www.gnome.org"

puts ws1.start_with? "www."
puts ws2.start_with? "www."

puts

puts ws1.end_with? ".com"
puts ws2.end_with? ".com"
```

#### Ruby formatting strings

Ruby has format specifiers. A format specifier determines how the string is going to look like. It begins with a % character. Format specifiers are put inside single or double quotes.

The format specifier has the following fields:

%[flags][field width][precision]conversion specifier

```ruby
puts "There are %d oranges in the basket." % 12
puts "There are %d oranges and %d apples in the basket." % [12, 10]
puts "There are %d apples." % 5
puts "I can see %i oranges." % 3
puts "The width of iPhone 3G is %f mm." % 62.1
puts "This animal is called a %s" % "rhinoceros."

website = "zetcode.com"

website.each_char do |c|
    print "#{c} has ASCII code %d\n" % c.ord
end

# decimal
puts "%d" % 300

# hexadecimal
puts "%x" % 300

# octal
puts "%o" % 300

# binary
puts "%b" % 300

# scientific
puts "%e" % (5/3.0)

=begin
300
12c
454
100101100
1.666667e+00
=end

puts 'Height: %f %s' % [172.3, 'cm']
puts 'Height: %.1f %s' % [172.3, 'cm']
```

Check more on [zetcode](http://zetcode.com/lang/rubytutorial/strings/)

<a name="statement"></a>

### RUBY STATEMENT

#### Ruby if statement

```ruby
age = 17
if age > 18
    puts "Driving license issued"
else
    puts "Driving license not permitted"
end

print "Enter a number: "
num = gets.to_i
if num < 0
    puts "#{num} is negative"
elsif num == 0
  puts "#{num} is zero"
elsif num > 0
  puts "#{num} is positive"
end
```

#### Ruby case statement

```ruby
print "Enter top level domain: "
domain = gets.chomp
case domain
  when "us"
    puts "United States"
  when "de"
    puts "Germany"
  when "sk"
    puts "Slovakia"
  when "hu"
    puts "Hungary"
  else
    puts "Unknown"
```

#### Ruby while, until statements

```ruby
i = 0
sum = 0
while i<10 do
  i +=1
  sum +=i
end
puts "The sum of 0..9 values is #{sum}"
```

```ruby
hoursLeft = 12
until hoursLeft == 0
  if hoursLeft ==1
    puts "There is #{hoursLeft} hour left"
  else
    puts "There are #{hoursLeft} hours left"
  end
  hoursLeft -= 1
end
```

#### Ruby for statement

```ruby
for i in 0..9 do
  puts "#{i}"
end

planets = ["Mercury", "Venus", "Earth", "Mars", "Jupiter",
  "Saturn", "Uranus", "Neptune"]

for i in 0...planets.length
    puts planets[i]
end
```

#### Ruby each method

```ruby
planets = ["Mercury", "Venus", "Earth", "Mars", "Jupiter",
  "Saturn", "Uranus", "Neptune"]

planets.each do |planet|
    puts planet
end
```

#### Ruby break, next statements

```ruby
while true

    r = 1 + rand(30)
    print "#{r} "

    if r == 22
        break
    end
end
```

#### Ruby redo statement

The redo statement restarts an iteration of a loop, without checking the loop condition.

```ruby
options = ["rock", "scissors", "paper"]

while true

    print <<TEXT
1 - rock
2 - scissors
3 - paper
9 - end game
TEXT

    val = gets.to_i

    r = rand(3) + 1

    if val == 9
        puts "End"
        exit
    end

    if ![1, 2, 3, 9].include?(val)
        puts "Invalid option"
        redo
    end

    computer = options[r-1]
    human = options[val-1]

    puts "I have #{computer}, you have #{human}"

    if val == r
        puts "Tie, next throw"
        redo
    end

    if val == 1 and r == 2
        puts "Rock blunts scissors, you win"

    elsif val == 2 and r == 1
        puts "Rock blunts scissors, you loose"

    elsif val == 2 and r == 3
        puts "Scissors cut paper, you win"

    elsif val == 3 and r == 2
        puts "Scissors cut paper, you loose"

    elsif val == 3 and r == 1
        puts "Paper covers rock, you win"

    elsif val == 1 and r == 3
        puts "Paper covers rock, you loose"

    end

end

```

<a name="array"></a>

### RUBY ARRAY

#### Declarations and loop

```ruby
num = [1,2,3,4,5]
num.each do |num|
  put num
end
```

#### Ruby array creationg

```ruby
nums = Arrays.new
nums.push 1
nums.push 2

a1 = Array.new
a2 = Array.new 3
a3 = Array.new 6, "coin"
a4 = Array.new [11]
a5 = Array.new (15) {|e| e*e}

puts [a1, a2, a3, a4, a5].inspect

integers = [1, 2, 3, 4, 5]
animals = %w( donkey dog cat dolphin eagle )
weights = Array.new
weights << 4.55 << 3.22 << 3.55 << 8.55 << 3.23

puts integers.inspect
puts animals.inspect
puts weights.inspect

various = [1, -1, "big", 3.4, Empty.new, nums, :two]
puts various.inspect
```

#### Nested array

```ruby
numbers = [1, 2, 3, [2, 4, 6, [11, 12]]]
puts numbers.length
puts numbers[0], numbers[1]

puts numbers[3][0]
puts numbers[3][1]

puts numbers[3][3][0]
puts numbers[3][3][1]

puts numbers.flatten!.inspect
```

#### Ruby printing array contents

```ruby
integers = [1, 2, 3, 4, 5]

puts integers
puts integers.inspect

integers.each do |e|
    puts e
end

integers.length.times do |idx|
  puts integers[idx]
end

integers.each_with_index do |num, idx|
    puts "value #{num} has index #{idx}"
end
```

#### Ruby array operations

```ruby
num1 = [1, 2, 3, 4, 5]
num2 = [6, 7, 8, 9, 10]

puts num1 + num2
puts num1.concat num2
```

```ruby
lts = %w{ a b c d e f}

puts lts.inspect
puts "Array has #{lts.length} elements"
puts "The first element is #{lts.first}"
puts "The last element is #{lts.last}"

puts lts.eql? lts.dup #return true. The dup method creates a shallow copy of an object.
puts lts.eql? lts.dup.delete_at(0)

lts.clear
puts lts.inspect
puts lts.empty?
```

#### Ruby modifying arrays

```ruby
lts = []

lts.insert 0, 'E', 'F', 'G' #The insert method inserts three elements into the lts array.
lts.push 'H' #The push method appends the elements to the array.
lts.push 'I', 'J', 'K'
lts << 'L' << 'M' #The << is a synonym for the push method.
lts.unshift 'A', 'B', 'C' #The unshift method prepends elements to the front of the array.
lts.insert(3, 'D') #the insert method inserts the 'D' character at a specific index.
```

```ruby
lts = %w{ a b c d e f g h}

lts.pop #The pop method removes the last element from the array.
lts.shift #The shift method removes the first element from the array.
lts.delete_at(0) #The delete_at deletes an element at a specific position.
lts.delete('d') #The delete method deletes a specific item from the array.
lts.clear #The clear method clears all the elements from the array.
```

Exlamation mark
The exclamation mark tells the programmer that the method will modify data. The exclamation mark itself does not have any effect. It is merely a naming convention.

```ruby
chars = %w{a b c d e}

reversed_chars = chars.reverse
puts reversed_chars.inspect #[e d c b a]
puts chars.inspect #[a, b, c,d,e]

reversed_chars = chars.reverse!
puts reversed_chars.inspect #[e d c b a]
puts chars.inspect #[e d c b a]
```

```ruby
numbers = [1, 2, 2, 2, 3, 4, 5, 8, 11]

puts numbers.index 2 #The index method returns the index of the array element.
puts numbers.index 11
puts numbers.rindex 2 #The rindex returns the index of the first element from the right.

puts numbers.include? 3 #The include? method checks if an element is present in the array.
puts numbers.include? 10

puts numbers.join '-' #The join method returns a string created from the array elements, separated by a provided separator.
puts numbers.uniq!.inspect #The uniq! method removes duplicate elements from the array.
```

#### Ruby reading array elements

```ruby
lts = %w{ a b c d e f g h}

puts lts.first
puts lts.last
puts lts.at(3)
puts lts[0]
puts lts[-1]
puts lts[0, 3].inspect
puts lts[2..6].inspect
puts lts[2...6].inspect

puts lts.values_at(1..5).inspect
puts lts.values_at(1, 3, 5).inspect
puts lts.values_at(1, 3, 5, 6, 8).inspect
puts lts.values_at(-1, -3).inspect

puts lts.fetch(0)
puts lts.fetch(-2)
puts lts.fetch(8, 'undefined')
puts lts.fetch(8) { |e| -2*e }
```

#### Ruby set operations

```ruby
A = [1, 2, 3, 4, 5]
B = [4, 5, 6, 7, 8]

union = A | B
isect = A & B
diff1  = A - B
diff2  = B - A
sdiff = (A - B) | (B - A)
```

#### Ruby array select, collect, map methods

```ruby
nums = [1, 3, 2, 6, 7, 12, 8, 15]

selected= nums.select do |e|
  e > 10
end

collected = nums.collect do |e|
  e < 10
end

mapped = nums.map do |e|
  e*2
end

#[12, 15]
#[true, true, true, true, true, false, true, false]
#[2, 6, 4, 12, 14, 24, 16, 30]
```

#### Ruby array ordering elements

```ruby
planets = %w{ Mercury Venus Earth Mars Jupiter
              Saturn Uranus Neptune Pluto }

puts "#{planets.sort}"
puts "#{planets.reverse}"
puts "#{planets.shuffle}"
```

<a name="hashes"></a>

### RUBY HASHES

#### Basic creation

- Create a new hashes

  ```ruby
  hash = Hash.new
  hash[1] = "Jane"
  hash[2] = "Thomas"

  names = Hash.new
  names.store(1, "Jane")
  names.store(2, "Thomas")
  names.store(3, "Rebecca")

  puts "The size of the hash is #{names.size}"

  puts names.keys.inspect
  puts names.values.inspect
  ```

#### Using key-value pairs

- Key-Value

  ```ruby
  domains = { "de" => "Germany",
            "sk" => "Slovakia",
            "hu" => "Hungary",
            "us" => "United States",
            "no" => "Norway"
          }

  puts domains["de"]
  puts domains["sk"]
  puts domains.has_key? "de"
  puts domains.include? "no"
  puts domains.key? "me"
  puts domains.member? "sk"

  puts domains.has_value? "Slovakia"
  puts domains.value? "Germany"

  stones = { 1 => "garnet", 2 => "topaz",
          3 => "opal", 4 => "amethyst"
        }

  puts stones.fetch 1
  puts stones[2]
  puts stones.values_at 1, 2, 3
  ```
 

#### Ruby hash iteration

```ruby
stones = { 1 => "garnet", 2 => "topaz",
         3 => "opal", 4 => "amethyst"
       }

stones.each { |k, v| puts "Key: #{k}, Value: #{v}" }
stones.each_key { |key| puts "#{key}" }
stones.each_value { |val| puts "#{val}" }
stones.each_pair { |k, v| puts "Key: #{k}, Value: #{v}" }
```
 ```ruby
  users = [
    {username: "john1", password: "123"},
    {username: "john2", password: "123"},
    {username: "john3", password: "123"},
    {username: "john4", password: "123"},
    {username: "john5", password: "123"},
  ]

  def auth_user(username, password, list_of_users)
    list_of_users.each do |user_record|
        if user_record[:username] == username && user_record[:password] == password
          return user_record
        else
          return "Credentials were not correct"
        end
      end
  end

  puts "Welcome to the autheticator"
  puts '-' * 100
  puts "This program will take input from the user and compare password"
  puts "If password is correct, you will get back the user object"

  attemps = 1
  while attemps < 4
    print "Username: "
    username = gets.chomp
    print "Password: "
    password= gets.chomp
    authentication = auth_user(username,password,users)
    puts authentication
    puts "Press n to quite or any other key to continue: "
    input = gets.chomp.downcase
    break if input =="n"
    attemps +=1
  end
  puts "You have exceeded the number of attempts"
```
Check more detail in [Viblo.asia](https://viblo.asia/p/hash-trong-ruby-rQOePNNOvYj)

#### Ruby deleting pairs in hash

- Delete pairs in hash with keyword reject and delete_if

  ```ruby
  names = Hash.new

  names[1] = "Jane"
  names[2] = "Thomas"
  names[3] = "Robert"
  names[4] = "Julia"
  names[5] = "Rebecca"

  names.delete 4
  names.shift
  puts names1.reject { |k, v| v =~ /R.*/ }
  puts names1
  puts names1.delete_if { |k, v| k<=3 }
  puts names1
  ```

#### Ruby adding elements to hashes

- Method merge of hash in Ruby

  ```ruby
  names1 = Hash.new

  names1[1] = "Jane"
  names1[2] = "Thomas"

  names2 = Hash.new

  names2[3] = "Robert"
  names2[4] = "Julia"

  names = names1.merge names2
  puts names

  names = names1.update names2
  puts names
  ```

<a name="oop"></a>

### RUBY OOP

#### Ruby constructor

- Using constructor with keyword initialize

  ```ruby
  class Being
    def initialize
        puts "Beings is created"
    end
  end

  Being.new

  class Person

    def initialize name
        @name = name
    end

    def get_name
        @name
    end

  end

  p1 = Person.new "Jane"
  p2 = Person.new "Beky"

  puts p1.get_name
  puts p2.get_name
  ```

#### Ruby constructor overloading

- Overloading method (declare the default variables)

  ```ruby
  class Person

    def initialize name="unknown", age=0
        @name = name
        @age = age
    end

    def to_s
        "Name: #{@name}, Age: #{@age}"
    end

  end

  p1 = Person.new
  p2 = Person.new "unknown", 17
  p3 = Person.new "Becky", 19
  p4 = Person.new "Robert"

  p p1, p2, p3, p4
  ```

#### Ruby methods

- How to use the method in Ruby

  ```ruby
  class Circle

    @@PI = 3.141592

    def initialize
        @radius = 0
    end

    def set_radius radius
        @radius = radius
    end

    def area
        @radius * @radius * @@PI
    end

  end

  c = Circle.new
  c.set_radius 5
  puts c.area

  ```

#### Ruby access modifiers

- Public access

  ```ruby
  class Some

      def method1
          puts "public method1 called"
      end

      public

      def method2
          puts "public method2 called"
      end

      def method3
          puts "public method3 called"
          method1
          self.method1
      end
  end

  s = Some.new
  s.method1
  s.method2
  s.method3
  ```

- Private access

  ```ruby
  class Some

      def initialize
          method1
          # self.method1
      end

      private

      def method1
          puts "private method1 called"
      end

  end

  s = Some.new
  # s.method1
  ```

- Protected access

  ```ruby
  class Some

    def initialize
        method1
        self.method1
    end

    protected

     def method1
         puts "protected method1 called"
     end

  end

  s = Some.new
  # s.method1
  ```

#### Ruby inheritance

- Inheritance is a way to form new classes using classes that have already been defined. The newly formed classes are called derived classes, the classes that we derive from are called base classes. Important benefits of inheritance are code reuse and reduction of complexity of a program. The derived classes (descendants) override or extend the functionality of base classes (ancestors).

  ```ruby
  class Being

      def initialize
          puts "Being class created"
      end
  end

  class Human < Being

    def initialize
        super
        #The super method calls the constructor of the parent class.
        puts "Human class created"
    end
  end

  Being.new
  Human.new
  ```

- In C# or Java, public and protected data members and methods are inherited; private data members and methods are not. In contrast to this, private data members and methods are inherited in Ruby as well. The visibility of data members and methods is not affected by inheritance in Ruby.

  ```ruby
  class Base

      def initialize
          @name = "Base"
      end

      private

      def private_method
          puts "private method called"
      end

      protected

      def protected_method
          puts "protected_method called"
      end

      public

      def get_name
          return @name
      end
  end

  class Derived < Base

        def public_method
            private_method
            protected_method
        end

  end

  d = Derived.new
  d.public_method
  puts d.get_name

  ```

#### Ruby super method

- The super method calls a method of the same name in the parent's class. If the method has no arguments it automatically passes all its arguments. If we write super() no arguments are passed to parent's method.

  ```ruby
  class Base

      def show x=0, y=0
          p "Base class, x: #{x}, y: #{y}"
      end
  end

  class Derived < Base

      def show x, y
          super
          super x
          super x, y
          super()
      end
  end
  d = Derived.new
  d.show 3, 3

  ```

  The super method without any arguments calls the parent's show method with the arguments that were passed to the show method of the Derived class: here, x=3 and y=3. The super() method passes no arguments to the parent's show method.

#### Ruby attribute accessors

All Ruby variables are private. It is possible to access them only via methods. These methods are often called setters and getters. Creating a setter and a getter method is a very common task. Therefore Ruby has convenient methods to create both types of methods. They are attr_reader, attr_writer and attr_accessor.

- The attr_reader creates getter methods.
- The attr_writer method creates setter methods and instance variables for this setters
- The attr_accessor method creates both getter, setter methods and their instance variables.

  ```ruby
  class Car

    attr_reader :name, :price
    attr_writer :name, :price

    def to_s
        "#{@name}: #{@price}"
    end
  end

  c1 = Car.new
  c2 = Car.new

  c1.name = "Porsche"
  c1.price = 23500

  c2.name = "Volkswagen"
  c2.price = 9500

  puts "The #{c1.name} costs #{c1.price}"

  p c1
  p c2
  ```

- As we already stated above, the attr_accessor method creates getter, setter methods and their instance variables.

  ```ruby
    class Book
    attr_accessor :title, :pages
    end

    b1 = Book.new
    b1.title = "Hidden motives"
    b1.pages = 255

    p "The book #{b1.title} has #{b1.pages} pages"
  ```

#### Ruby class constants

- Ruby enables you to create class constants. These constants do not belong to a concrete object. They belong to the class. By convention, constants are written in uppercase letters.

  ```ruby
  class MMath

    PI = 3.141592
  end

  puts MMath::PI

  ```

- Each object has a to_s method. It returns a string representation of the object. Note that when the puts method takes an object as a parameter, the to_s of the object is being called.

  ```ruby
  class Being

    def to_s
        "This is Being class"
    end
  end

  b = Being.new
  puts b.to_s
  puts b
  ```

#### Ruby operator overloading

- Operator overloading is a situation where different operators have different implementations depending on their arguments.

  ```ruby
  class Circle

    attr_accessor :radius

    def initialize r
        @radius = r
    end

    #We define a method with a + name. The method adds the radiuses of two circle objects.
    def +(other)
        Circle.new @radius + other.radius
    end

    def to_s
        "Circle with radius: #{@radius}"
    end

  end

  c1 = Circle.new 5
  c2 = Circle.new 6
  c3 = c1 + c2

  p c3

  ```

#### Ruby class methods

- Ruby methods can be divided into class methods and instance methods. Class methods are called on a class. They cannot be called on an instance of a class.

  ```ruby
  class Circle

    def initialize x
        @r = x
    end

    # Class methods in Ruby like a static method in C# or Java
    def self.info
       "This is a Circle class"
    end

    def area
        @r * @r * 3.141592
    end

  end

  p Circle.info
  c = Circle.new 3
  p c.area
  ```

- 3 ways to create an instance method in Ruby

  ```ruby
  class Wood

    def info
       "This is a wood object"
    end
  end

  wood = Wood.new
  p wood.info

  class Brick

  attr_accessor :info
  end

  brick = Brick.new
  brick.info = "This is a brick object"
  p brick.info

  class Rock

  end

  rock = Rock.new

  def rock.info
  "This is a rock object"
  end

  p rock.info
  ```

#### Ruby polymorphism

- Polymorphism is the process of using an operator or function in different ways for different data input. In practical terms, polymorphism means that if class B inherits from class A, it doesn't have to inherit everything about class A; it can do some of the things that class A does differently.

  ```ruby
  class Animal

    def make_noise
        "Some noise"
    end

    def sleep
        puts "#{self.class.name} is sleeping."
    end

  end

  class Dog < Animal

      def make_noise
          'Woof!'
      end

  end

  class Cat < Animal

      def make_noise
          'Meow!'
      end
  end

  [Animal.new, Dog.new, Cat.new].each do |animal|
    puts animal.make_noise
    animal.sleep
  end
  ```

#### Ruby modules

- A Ruby Module is a collection of methods, classes, and constants. Modules are similar to classes with a few differences. Modules cannot have instances and cannot subclasses.

  ```ruby
  module Forest

    class Rock ; end
    class Tree ; end
    class Animal ; end

  end

  module Town

    class Pool ; end
    class Cinema ; end
    class Square ; end
    class Animal ; end

  end

  p Forest::Tree.new
  p Forest::Rock.new
  p Town::Cinema.new

  p Forest::Animal.new
  p Town::Animal.new
  ```

  ```ruby
  module Device
    def switch_on ; puts "on" end
    def switch_off ; puts "off" end
  end

  module Volume
      def volume_up ; puts "volume up" end
      def vodule_down ; puts "volume down" end
  end

  module Pluggable
      def plug_in ; puts "plug in" end
      def plug_out ; puts "plug out" end
  end

  class CellPhone
      include Device, Volume, Pluggable

      def ring
          puts "ringing"
      end
  end

  cph = CellPhone.new
  cph.switch_on
  cph.volume_up
  cph.ring
  ```

#### Ruby exceptions

- rescue keyword

  ```ruby
  x = 35
  y = 0

  begin
      z = x / y
      puts z
  rescue => e
      puts e
      p e
  end
  #In the code following the rescue keyword, we deal with an exception. In this case, we print the error message to the console. The e is an exception object that is created when the error occurs.
  ```

  ```ruby
  age = 17

  begin
      if age < 18
          raise "Person is a minor"
      end

      puts "Entry allowed"
  rescue => e
      puts e
      p e
      exit 1
  end
  ```

  ```ruby
  begin
    f = File.open("stones", "r")

    while line = f.gets do
        puts line
    end

  rescue => e
      puts e
      p e
  ensure
      f.close if f
  end
  #In the ensure block we close the file handler. We check if the handler exists because it might not have been created. Allocated resources are often placed in the ensure block.
  ```

---

Reference in [zetcode](zetcode.com)

<a name="license"></a>

# 📃 License

MIT © [Tien-Duy NGUYEN](https://github.com/tienduy-nguyen)

## :baby_chick:
