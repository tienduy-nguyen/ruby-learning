# Using database in Rails

## Important commandes

```bash
rake db:create
rake db:migrate
```

```bash
rails console #Enter in console environment irb
rails generate model User
rails db:migrate
rails g migration CreateUser
rails destroy migration CreateUser
rails generate model User email:string is_admin:boolean
rails db:migrate:status
rails db:rollback

```


## Configuration


### For sqlite3

`database.yml` by default for sqlite3

```yml
# SQLite version 3.x
#   gem install sqlite3
#
#   Ensure the SQLite 3 gem is defined in your Gemfile
#   gem 'sqlite3'
#
default: &default
  adapter: sqlite3
  pool: <%= ENV.fetch("RAILS_MAX_THREADS") { 5 } %>
  timeout: 5000

development:
  <<: *default
  database: db/development.sqlite3

# Warning: The database defined as "test" will be erased and
# re-generated from your development database when you run "rake".
# Do not set this db to the same as development or production.
test:
  <<: *default
  database: db/test.sqlite3

production:
  <<: *default
  database: db/production.sqlite3

```

### For mysql

```yml
default: &default
  adapter: mysql2
  database: projectmeal_dev
  username: root
  password: admin
  host: localhost
  port: 3306

development:
  <<: *default
  database: projectmeal_dev

# Warning: The database defined as "test" will be erased and
# re-generated from your development database when you run "rake".
# Do not set this db to the same as development or production.
test:
  <<: *default
  database: projectmeal_dev

production:
  <<: *default
  adapter: mysql2
  encoding: utf8
  database: <%= ENV['RDS_DB_NAME'] %>
  username: <%= ENV['RDS_USERNAME'] %>
  password: <%= ENV['RDS_PASSWORD'] %>
  host: <%= ENV['RDS_HOST_NAME'] %>
  port: <%= ENV['RDS_PORT'] %>

```

## Migration

- Generate migration
  ```
  rails generate migration name_migration
  rails g migration CreateUserTable
  ```

## Model

- Generate model
  ```
  rails generate model OrgCompany

  ```

- Enter in the environment irb for testing the 
```
rails console
```
- We use the model to interact with database
  ```irb
  p = Post.find(1)
  p.name
  p[:name]
  p.content
  p.save #update
  p.destroy

  p = Post.new # Create new post instance
  P.name = "Hello"
  p.save

  p = Post.create(name:'Post3', content: 'This is a test')

  post.id

  p = Post.find(3)

  p.update(name="Post 3 change name")

  post = Post.all

  Post.order(:name)

  Post.limit(1)

  Post.order(:name).limit(1)

  Post.where(name: 'Hello')

  Post.where(name: 'Hello').update_all(name:'welcome')

  Post.where(name: 'Hello').destroy_all

  exit
  ```

- Create model.rb
  
  org_company.rb
  ```ruby
  class orgCompany < ActiveRecord::Base
    has_and_belong_to_many :org_contacts
    has_many :org_persons
    has_many :org_products
  end
  ```

## Methods of models

- #all
- #new
- #save
- #create
- #update(atribute:value)
- #last
- #first
- #find(id)
- #find_by(attribue:value)
- #where(attribute:value)
- #destroy


## Active record associations

[Ruby Record associations](https://guides.rubyonrails.org/association_basics.html)

Rails supports six types of associations:

- belongs_to
- has_one
- has_many
- has_many :through
- has_one :through
- has_and_belongs_to_many

Foreign_key
```
create_table :accounts do |t|
  t.belongs_to :supplier, index: { unique: true }, foreign_key: true
  # ...
end
```