# Ruby on Rails basics

### Rails: Create a blog application

- Create a project
  ```bash
  rails new name_project (blog)
  ```
  ```bash
  cd blog
  ```
- Starting up the web server
  ```bash
  rails server
  ```
- Create a controller
  ```bash
  rails generate controller name_controller (Pages)
  ```

Check out detail in [rubyonrails.org](https://guides.rubyonrails.org/getting_started.html)


### Some rails shortcuts
| Full command   | Shortcut |
| :------------- | :------- |
| rails server   | rails s  |
| rails console  | rails c  |
| rails test     | rails t  |
| bundle install | bundle   |

### Undo the things in rails
- Undo migration
  
  ```bash
  rails g migration name_migration
  rails destroy migration name_migration
  ```
- Undo controller
  ```bash
  rails generate controller StaticPages home help
  rails destroy controller StaticPages home help
  ```
- Undo model
  ```bash
  rails generate model User name:string email:string
  rails destroy model User
  ```
- Undo db:migrate
  ```bash
  rails db:migrate
  rails db:rollback
  ```
- To go all way back to the beginning, we can use
  ```bash
  rails db:migrate VERSION=0
  ```
### Rails testing

```bash
rails test
```
